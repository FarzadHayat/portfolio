---
title: "IPDb (Internet Pen Database)"
tags: ['Python', 'Flask', 'SQLite', 'JavaScript', 'HTML', 'CSS', 'Web']
date: 2020-11-09
draft: false
cover:
    image: "/portfolio/images/ipdb.webp"
    hidden: true
---
A Flask web server made in 2020 to store an online database of pens.
The purpose is to inform the user about different pens brands, pen features, and pens models.
There is a sample collection of brands, tags, and pens to showcase the website features.
The user is able to interact with the database by adding new brands or modifying/deleting existing ones.
This was a solo school project made for the year 13 programming class.
This website was made using Python, Flask, SQLite, HTML/CSS, and JavaScript.

[GitLab](https://gitlab.com/FarzadHayat/pen-website)

{{< rawhtml >}}
<table>
    <tr>
        <td style="border:0px;">
            {{< inTextImg url="/portfolio/images/ipdb/1.webp" height="50%">}}
        </td>
        <td style="border:0px;"> 
            {{< inTextImg url="/portfolio/images/ipdb/2.webp" height="50%">}}
        </td>
    </tr>
    <tr>
        <td style="border:0px;">
            {{< inTextImg url="/portfolio/images/ipdb/3.webp" height="50%">}}
        </td>
        <td style="border:0px;"> 
            {{< inTextImg url="/portfolio/images/ipdb/4.webp" height="50%">}}
        </td>
    </tr>
    <tr>
        <td style="border:0px;">
            {{< inTextImg url="/portfolio/images/ipdb/5.webp" height="50%">}}
        </td>
        <td style="border:0px;">
            {{< inTextImg url="/portfolio/images/ipdb/6.webp" height="50%">}}
        </td>
    </tr>
</table>
{{< /rawhtml >}}
