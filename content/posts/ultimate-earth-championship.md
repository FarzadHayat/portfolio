---
title: "Ultimate Earth Championship"
tags: ['Java', 'Windows', 'Eclipse', 'Swing', 'JUnit', 'UML']
date: 2023-06-05
draft: false
cover:
    image: "/portfolio/images/ultimate-earth-championship.webp"
    hidden: true
---
A turn-based player-vs-ai game made for Windows.


Aliens have invaded Earth and ressurected the greatest of humanity back to life to battle each other in the intergalactic sports tournament.
You are to create a team of up to 9 champions, provide them with weapons, and fight against other teams in a checkered style battle arena.
The other teams intelligently fight each other and make purchases from the market in the background to create a realistic battling system.


This game was made in Semester 1 of 2023 as a university course project between Farzad Hayatbakhsh and Oliver Coates.
This game was made using Java with the Swing GUI Toolkit in the Eclipse IDE.
We utilized JUnit for unit testing and UML diagrams for planning.


[GitLab Repo](https://gitlab.com/FarzadHayat/ultimate-earth-championship)


You can download the Git repo as a zip and follow the instrutions in the **README.md** to run the game.


### Folder Index
 - **UltimateEarthChampionship/** - Contains Eclipse project source code including unit tests
 - **doc/** - Contains compiled Javadoc documentation for the project
 - **uml/** - Contains UML use case and class diagrams in both PNG format
 - **Report.pdf** - Project report
 - **README.md** - The file containing instructions on how to open and run the project
 - **fha62_oco36_SportsTournament.jar** - Runnable jar file


{{< rawhtml >}}
<table>
    <tr>
        <td style="border:0px;">
            {{< inTextImg url="/portfolio/images/ultimate-earth-championship/1.webp" height="50%">}}
        </td>
        <td style="border:0px;"> 
            {{< inTextImg url="/portfolio/images/ultimate-earth-championship/2.webp" height="50%">}}
        </td>
    </tr>
    <tr>
        <td style="border:0px;">
            {{< inTextImg url="/portfolio/images/ultimate-earth-championship/3.webp" height="50%">}}
        </td>
        <td style="border:0px;"> 
            {{< inTextImg url="/portfolio/images/ultimate-earth-championship/4.webp" height="50%">}}
        </td>
    </tr>
    <tr>
        <td style="border:0px;">
            {{< inTextImg url="/portfolio/images/ultimate-earth-championship/5.webp" height="50%">}}
        </td>
        <td style="border:0px;">
            {{< inTextImg url="/portfolio/images/ultimate-earth-championship/6.webp" height="50%">}}
        </td>
    </tr>
    <tr>
        <td style="border:0px;">
            {{< inTextImg url="/portfolio/images/ultimate-earth-championship/7.webp" height="50%">}}
        </td>
        <td style="border:0px;">
            {{< inTextImg url="/portfolio/images/ultimate-earth-championship/8.webp" height="50%">}}
        </td>
    </tr>
</table>
{{< /rawhtml >}}
